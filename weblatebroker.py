#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""Weblate broker for Bitbucket."""

### LICENSE ###
# Copyright © 2012, Frederik "Freso" Sandberg Olesen
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import urllib
from urlparse import urlsplit, urlunsplit, urljoin
import unittest
# Import built-in JSON module if Django is not available
try:
    from django.utils import simplejson as json
except ImportError:
    import json
# Make stub BaseBroker class if brokers module is not available
try:
    from brokers import BaseBroker
except ImportError:
    class BaseBroker(object):
        """Stub BaseBroker class."""
        def get_local(self, arg, someclass):
            return someclass()


class URLOpener(urllib.FancyURLopener):
    """Not really sure what this is good for..."""
    version = 'bitbucket.org'


class WeblateBroker(BaseBroker):
    """Bitbucket broker class for Weblate."""

    def get_url(self, payload_url):
        """Verify set URL and return URL for submission."""
        url = payload_url
        hook_path = '/hooks/bitbucket/'

        # The protocols support by the broker.
        supported_url_schemes = ['http', 'https']

        if not url:
            raise StandardError("The service's URL argument is not set.")

        split_url = urlsplit(url, scheme='http')
        if split_url.scheme not in supported_url_schemes:
            raise StandardError("Only HTTP and HTTPS URL schemes are supported.")
        if not split_url.netloc and split_url.path:
            split_url_list = list(split_url)
            split_url_list[1], split_url_list[2] = (split_url.path, '')
            url = urlunsplit(split_url_list)
        else:
            url = split_url.geturl()

        url = urljoin(url, hook_path)

        return url

    def handle(self, payload):
        """Handle the payload submission to Weblate.

        payload['service'] needs to have an 'url' key for the Weblate site to
        send the data."""

        # Get the configured Weblate URL for the project.
        url = self.get_url(payload['service']['url'])

        # Remove data that shouldn't be forwarded to Weblate.
        del payload['service']
        del payload['broker']

        # Generate the payload to post.
        post_load = {'payload': json.dumps(payload)}

        # Send the payload.
        opener = self.get_local('opener', URLOpener)
        opener.open(url, urllib.urlencode(post_load))
        # TODO: Check for errors in submission?


class WeblateBrokerTestCase(unittest.TestCase):
    """Test the WeblateBroker class."""

    def setUp(self):
        """Set up WeblateBrokerTestCase.

        This basically just makes an instance of WeblateBroker readily
        available for the tests.
        """
        self.broker = WeblateBroker()

    def self_test(self):
        """Test WeblateBroker itself."""
        self.assertTrue(self.broker.__doc__, "no docstring defined.")

    def get_url_test(self):
        """Test WeblateBroker.get_url()."""
        get_url = self.broker.get_url

        # It should raise an exception if no argument was provided.
        self.assertRaises(StandardError, get_url, '')

        # These should all raise an exception:
        self.assertRaises(StandardError, get_url, 'ftp://example.com/')
        self.assertRaises(StandardError, get_url, 'file:///home/weblate/')

        # These should all resolve to http://hosted.weblate.org/hooks/github/
        target_url = 'http://hosted.weblate.org/hooks/github/'
        self.assertEqual(get_url('http://hosted.weblate.org/'), target_url)
        self.assertEqual(get_url('hosted.weblate.org'), target_url)
        self.assertEqual(get_url('HTTP://hosted.weblate.org//'), target_url)
        self.assertEqual(get_url('HTTP://hosted.weblate.org/index.html'), target_url)

    def handle_test(self):
        """Test WeblateBroker.handle()."""
        pass


if __name__ == '__main__':
    unittest.main()
